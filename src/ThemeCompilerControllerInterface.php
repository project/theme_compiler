<?php

namespace Drupal\theme_compiler;

use Drupal\compiler\CompilerContextInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines the interface of the theme compiler controller.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @internal
 */
interface ThemeCompilerControllerInterface {

  /**
   * Return a response containing the result of compiling the requested context.
   *
   * @param \Drupal\compiler\CompilerContextInterface $theme_compiler_context
   *   A compiler context used to define a compilation.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response containing the result of the compiled context.
   */
  public function serve(CompilerContextInterface $theme_compiler_context): Response;

}
