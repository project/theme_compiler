<?php

namespace Drupal\theme_compiler;

use Drupal\compiler\Plugin\CompilerPluginManagerInterface;
use Drupal\compiler\RefineableCompilerContextInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use function Sabre\Uri\normalize;
use function Sabre\Uri\resolve;

/**
 * An on-demand compiler for the dynamic resource(s) provided by this module.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @internal
 */
class Compiler implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The sandbox path used to store compiler targets.
   *
   * Callers are not allowed to escape this path; all target paths will be
   * resolved relative to this path and will fall beneath this path.
   *
   * The trailing slash is important to ensure that a similarly-named parent
   * cannot be used to escape the sandbox.
   *
   * @var string
   */
  const SANDBOX = 'public://theme-compiler-assets/';

  /**
   * The cache tags invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The compiler plugin manager service.
   *
   * @var \Drupal\compiler\Plugin\CompilerPluginManagerInterface
   */
  protected $compilerManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The route provider service.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The theme initialization service.
   *
   * @var \Drupal\Core\Theme\ThemeInitializationInterface
   */
  protected $themeInitialization;

  /**
   * The theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Constructs a CompilerController object.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator service.
   * @param \Drupal\compiler\Plugin\CompilerPluginManagerInterface $compiler_plugin_manager
   *   The compiler plugin manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider service.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler service.
   * @param \Drupal\Core\Theme\ThemeInitializationInterface $theme_initialization
   *   The theme initialization service.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager service.
   */
  public function __construct(CacheTagsInvalidatorInterface $cache_tags_invalidator, CompilerPluginManagerInterface $compiler_plugin_manager, FileSystemInterface $file_system, MessengerInterface $messenger, ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $logger_factory, RouteProviderInterface $route_provider, ThemeHandlerInterface $theme_handler, ThemeInitializationInterface $theme_initialization, ThemeManagerInterface $theme_manager) {
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->compilerManager = $compiler_plugin_manager;
    $this->fileSystem = $file_system;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    $this->loggerFactory = $logger_factory;
    $this->routeProvider = $route_provider;
    $this->themeHandler = $theme_handler;
    $this->themeInitialization = $theme_initialization;
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache_tags.invalidator'),
      $container->get('plugin.manager.compiler'),
      $container->get('file_system'),
      $container->get('messenger'),
      $container->get('module_handler'),
      $container->get('logger.factory'),
      $container->get('router.route_provider'),
      $container->get('theme_handler'),
      $container->get('theme.initialization'),
      $container->get('theme.manager'),
    );
  }

  /**
   * Compile the supplied context and return the result.
   *
   * Extensions (specifically modules & themes) are allowed to hook this process
   * to modify the compiler plugin instance and compiler context before the
   * compilation process has begun.
   *
   * @param \Drupal\compiler\RefineableCompilerContextInterface $context
   *   A compiler context used to define a compilation process.
   *
   * @see hook_theme_compiler_alter()
   * @see hook_theme_compiler_TYPE_alter()
   *
   * @throws \Drupal\theme_compiler\CompilerException
   *   If an exception occurs during compilation. This exception will be
   *   constructed with the previous exception(s).
   *
   * @return mixed
   *   The content of the compilation result.
   */
  protected function compile(RefineableCompilerContextInterface $context) {
    // Attempt to fetch the compiler context & plugin for this compilation.
    $plugin = $this->compilerManager->createInstance($context->getCompiler());

    // Allow modules to alter the compiler & its context before execution.
    $this->moduleHandler->alter([
      'theme_compiler',
      "theme_compiler_{$context->getCompiler()}",
    ], $plugin, $context);

    // Allow themes to alter the compiler & its context before execution.
    //
    // Normally only the current theme is given an opportunity to alter the
    // compilation process, so we have to iterate over all enabled themes
    // manually to give them an opportunity to modify compilation.
    //
    // @see \Drupal\Core\Theme\ThemeManagerInterface::alter()
    // @see \Drupal\Core\Theme\ThemeManagerInterface::alterForTheme()
    foreach ($this->themeHandler->listInfo() as $name => $extension) {
      // Fetch an "active theme" instance for this theme.
      //
      // This might be a bit of a misnomer in Drupal core; the theme isn't
      // necessarily "active" (i.e., visible on the current page).
      $active_theme = $this->themeInitialization->initTheme($name);

      // Allow this theme to alter the compilation process.
      $this->themeManager->alterForTheme($active_theme, [
        'theme_compiler',
        "theme_compiler_{$context->getCompiler()}",
      ], $plugin, $context);
    }

    try {
      // Compile the context and return the result of the compilation.
      return $plugin->compile($context);
    }
    catch (\Exception $previous) {
      // Wrap all exceptions inside an exception for compilation errors.
      //
      // This wrapper exception will help downstream code when handling
      // compilation errors by reducing catch type complexity.
      throw new CompilerException('An error occurred during compilation', 0, $previous);
    }
  }

  /**
   * Compile the supplied context and save the result to a file on success.
   *
   * This is a destructive operation; the supplied target path will be replaced.
   *
   * @param \Drupal\compiler\RefineableCompilerContextInterface $context
   *   A compiler context used to define a compilation process.
   * @param string $target
   *   The target file used to save the result. This file path will be resolved
   *   relative to the sandbox path before it's used.
   *
   * @throws \RuntimeException
   *   If the output directory could not be prepared or the compiler result
   *   could not be saved to the specified file name.
   *
   * @see self::SANDBOX
   *   The sandbox path that constrains the supplied target path.
   *
   * @return string
   *   An absolute path to the resulting file.
   */
  protected function compileAndSave(RefineableCompilerContextInterface $context, string $target): string {
    // Attempt to resolve the supplied target path relative to the sandbox.
    $target = $this->normalizeAndResolveTargetPath($target);
    $dir = dirname($target);

    // Attempt to prepare the target's parent directory.
    if (!$this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      throw new \RuntimeException('Unable to prepare output directory');
    }

    // Compile the supplied context for storage.
    $compiled = $this->compile($context);

    // Attempt to compile the context & save the result to the target file.
    if (!($output = $this->fileSystem->saveData($compiled, $target, FileSystemInterface::EXISTS_REPLACE))) {
      throw new \RuntimeException('Unable to save compiler result');
    }

    // Invalidate any cached data associated with this target.
    $hash = hash('sha384', $target);
    $this->cacheTagsInvalidator->invalidateTags([
      "theme_compiler_asset:{$hash}",
      'library_info',
    ]);

    // Return the resulting file path to the caller.
    return $output;
  }

  /**
   * Compile & save all theme compiler assets to the file system.
   *
   * This method is "runtime-safe," meaning it shouldn't generate any
   * user-facing exceptions; any exceptions that occur will be silently logged.
   */
  public function compileAssets(): void {
    foreach ($this->getThemeCompilerRouteContexts() as $context) {
      try {
        $metadata = $context->getOption('theme_compiler');

        if (!is_array($metadata) || !array_key_exists('theme', $metadata) || !array_key_exists('path', $metadata)) {
          throw new \InvalidArgumentException('The compiler context is missing the required theme compiler metadata');
        }

        $this->compileAndSave($context, "{$metadata['theme']}/{$metadata['path']}");
      }
      catch (\Exception $exception) {
        // If this is a compiler exception, report the previous exception.
        if ($exception instanceof CompilerException) {
          $exception = $exception->getPrevious();
        }

        // Log this exception then add a non-fatal error for this response.
        $this->messenger->addError($this->t('An unexpected error occurred. Check the report log for additional information.'));
        Error::logException($this->loggerFactory->get('theme_compiler'), $exception);
      }
    }
  }

  /**
   * Get a list of contexts associated with applicable theme compiler routes.
   *
   * First, a list of all routes is retrieved from the route provider. Next,
   * each route is then mapped to its compiler context (should one exist).
   * Finally, the list of values is reduced to contain only valid contexts.
   *
   * @return \Drupal\compiler\RefineableCompilerContextInterface[]
   *   A list of refineable compiler contexts.
   */
  public function getThemeCompilerRouteContexts(): array {
    $routes = iterator_to_array($this->routeProvider->getAllRoutes());

    // Fetch a list of contexts as defined by this module.
    $contexts = array_filter(array_map(function (Route $route) {
      // For routes without an associated compiler context, NULL is returned.
      return $route->getDefault('theme_compiler_context');
    }, $routes), function ($context) {
      return $context instanceof RefineableCompilerContextInterface;
    });

    return $contexts;
  }

  /**
   * Resolve the supplied target path relative to the sandbox path.
   *
   * @param string $target
   *   The target path to resolve.
   *
   * @throws \InvalidArgumentException
   *   If the resulting target path isn't beneath the sandbox path.
   *
   * @return string
   *   The resolved path.
   */
  public function normalizeAndResolveTargetPath(string $target): string {
    // Attempt to resolve the supplied target path relative to the sandbox.
    $target = resolve(self::SANDBOX, normalize($target));

    // Ensure that the resulting target path is beneath the sandbox path.
    if (strpos($target, self::SANDBOX) !== 0 || $target === self::SANDBOX) {
      throw new \InvalidArgumentException('Invalid target path; the resulting path must fall beneath ' . self::SANDBOX);
    }

    return $target;
  }

}
