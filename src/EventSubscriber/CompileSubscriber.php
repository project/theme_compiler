<?php

namespace Drupal\theme_compiler\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\theme_compiler\Compiler;
use Drupal\theme_compiler\Event\OnDemandCompileEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * An event subscriber used to trigger (re)compilation of this module's assets.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @internal
 */
class CompileSubscriber implements EventSubscriberInterface {

  /**
   * The compiler service.
   *
   * @var \Drupal\theme_compiler\Compiler
   */
  protected $compiler;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The typed config manager service.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * Constructs an CompileSubscriber object.
   *
   * @param \Drupal\theme_compiler\Compiler $compiler
   *   The compiler service.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager service.
   */
  public function __construct(Compiler $compiler, ThemeHandlerInterface $theme_handler, TypedConfigManagerInterface $typed_config_manager) {
    $this->compiler = $compiler;
    $this->themeHandler = $theme_handler;
    $this->typedConfigManager = $typed_config_manager;
  }

  /**
   * Run the compilation process for this module.
   */
  public function compile() {
    $this->compiler->compileAssets();
  }

  /**
   * Get a list of configuration dependencies.
   *
   * The settings for any theme which defines a theme compiler asset, or the
   * settings for any themes that depend on such a theme, will be included.
   *
   * @return string[]
   *   A list of configuration dependencies.
   */
  protected function getConfigDependencies() {
    $dependencies = [];
    $themes = [];

    foreach ($this->compiler->getThemeCompilerRouteContexts() as $context) {
      if ($theme = $context->getOption('theme_compiler')['theme'] ?? NULL) {
        $themes[$theme] = $theme;
      }
    }

    $info = $this->themeHandler->listInfo();
    $themes = array_map(function ($theme) use ($info) {
      $result[$theme] = "{$theme}.settings";

      foreach (array_keys($info[$theme]->required_by) as $dependant) {
        $result[$dependant] = "{$dependant}.settings";
      }

      return $result;
    }, $themes);

    foreach ($themes as $dependants) {
      foreach ($dependants as $dependant) {
        $dependencies[$dependant] = $dependant;
      }
    }

    return array_values($dependencies);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::DELETE => 'onConfigChange',
      ConfigEvents::SAVE => 'onConfigChange',
      OnDemandCompileEvent::class => 'compile',
    ];
  }

  /**
   * Check whether the name of the changed config is applicable.
   *
   * This check guards against potentially costly router rebuilds that could
   * exhaust the working memory in a memory-restricted environment.
   *
   * @param string $name
   *   The config name to check.
   *
   * @return bool
   *   TRUE if the name of the changed config is applicable, FALSE otherwise.
   */
  protected function isApplicableConfigChange(string $name): bool {
    // The config name should follow the expected '{THEME}.settings' pattern.
    if (preg_match('/^(?P<theme>.*)\\.settings$/', $name, $matches)) {
      // The potential theme name should be a valid theme.
      return $this->themeHandler->themeExists($matches['theme']);
    }

    return FALSE;
  }

  /**
   * Compile all assets provided by this module on configuration changes.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The configuration change event.
   */
  public function onConfigChange(ConfigCrudEvent $event) {
    $name = $event->getConfig()->getName();

    // Only respond to config changes which are likely to be theme settings.
    if ($this->isApplicableConfigChange($name)) {
      // Check if the changed config is a compilation dependency.
      if (in_array($name, $this->getConfigDependencies(), TRUE)) {
        $this->compile();
      }
    }
  }

}
