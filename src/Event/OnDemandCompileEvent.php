<?php

namespace Drupal\theme_compiler\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * An event to trigger the compilation of all assets provided by this module.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class OnDemandCompileEvent extends Event {

}
